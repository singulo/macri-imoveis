var maxResultado = 12;

var imovelVisualizacao = $('.previa-imovel').clone();

$(document).ready(function(){
    if(window.location.pathname == '/imovel/pesquisar' || window.location.pathname == '/novo/imovel/pesquisar')
    {
        var pag = getParameterByName('pagina', location.href);

        pesquisar(pag, pag != null);
    }

    $('#ordenar_por').on('change', function(){
        pesquisar(0);
    });
});

function pesquisar(pagina, forcar_total)
{
    if(window.location.pathname != '/imovel/pesquisar' && window.location.pathname != '/novo/imovel/pesquisar')
    {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) )
            $('#filtro-imovel-mobile').submit();
        else
            $('#filtro-imovel').submit();
    }
    else
    {
        var msg_encontrados = '';
        if(pagina > 0)
            msg_encontrados = $('#msg-resultados-encontrados').html();

        $('#msg-resultados-encontrados').html('Buscando <strong>imóveis</strong>, aguarde ...');

        $('.imoveis-resultado-pesquisa').children().fadeOut(800, function(){
            $(this).remove();
        });

        filtro = {};

        filtro.pagina = pagina;

        if(forcar_total)
            filtro.forcar_total = 1;

        filtro.limite = maxResultado;
        filtro.ordenacao = $('#ordenar_por').val();

        //ATUALIZA URL SEM RECARREGAR A PÁGINA

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            window.history.pushState('', document.title, $('#base_url').val() + 'imovel/pesquisar?' + $.param($('#filtro-imovel-mobile').jsonify()) + '&' + $.param(filtro));
            filtro.filtro = $('#filtro-imovel-mobile').jsonify();
            console.log(filtro.filtro );
        }else{
            window.history.pushState('', document.title, $('#base_url').val() + 'imovel/pesquisar?' + $.param($('#filtro-imovel').jsonify()) + '&' + $.param(filtro));
            filtro.filtro = $('#filtro-imovel').jsonify();
        }

        ajaxPost(
            filtro,
            base_url_filial('imovel/buscar', true),
            {
                successCallback: function (data) {
                    $.each(data.imoveis, function (key, imovel) {
                        $('.imoveis-resultado-pesquisa').append(montar_imovel(imovel));
                    });

                    if (data.total != undefined && data.total > 0)
                    {
                        $('#msg-resultados-encontrados').html('<strong>Encontramos ' + data.total + '</strong> <br> <small>ofertas em nosso site.</small>');
                        atualizar_paginacao(parseInt(filtro.pagina) + 1, data.total);
                    }
                    else if(data.total != undefined)
                    {
                        $('#msg-resultados-encontrados').html('Nada foi encontrado.');
                        atualizar_paginacao(parseInt(filtro.pagina) + 1, 0);
                    }
                    else
                        $('#msg-resultados-encontrados').html(msg_encontrados);
                },
                failureCallback: function (data) {
                    alertify.error('Ocorreu um erro ao obter os imóveis.')
                },
                errorCallback: function (jqXhr, textStatus, errorThrown) {
                    alertify.error('Ocorreu um erro ao obter os imóveis. Tente novamente mais tarde.')
                },
                completeCallback: function () {
                }
            }
        );
    }
}

function montar_imovel(imovel)
{
    novaDiv = imovelVisualizacao.clone();

    novaDiv.find('img').attr('src', $('#filial_fotos_imoveis').val() + imovel.foto);

    novaDiv.find('.imovel-url').attr('href', imovel_url(imovel));

    novaDiv.find('.imovel-tipo').text(imoveis_tipos[imovel.id_tipo].tipo);

    var detalhes = [];

    if(imovel.dormitorios > 0 )
        detalhes.push(imovel.dormitorios + ' dorm.');

    if(imovel.area_total > 0 )
        detalhes.push(imovel.area_total + 'm²');

    if(imovel.garagem > 0)
        detalhes.push(imovel.garagem + ' vagas');

    if(imovel.suites > 0)
        detalhes.push(imovel.suites + ' suites');

    novaDiv.find('.detalhes').text(detalhes.join(' | '));


    novaDiv.find('.finalidade').text(finalidades[imovel.finalidade]);

    novaDiv.find('.ribbon').addClass(finalidades[imovel.finalidade].toLowerCase());

    novaDiv.find('.codigo').text('cód: ' + imovel.id);

    novaDiv.find('.valor-imovel').html(exibir_valor_imovel(imovel, 'R$'));

    novaDiv.show();

    return novaDiv;
}

function atualizar_paginacao(paginaAtual, items)
{
    $('#pagination').pagination({
        items: items,
        itemsOnPage: maxResultado,
        cssStyle: 'light-theme',
        nextText: '',
        prevText: '',
        currentPage: paginaAtual,
        onPageClick : function(pageNumber){
            pesquisar((pageNumber-1), maxResultado);
        }
    });
}