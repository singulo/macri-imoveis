<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.png'); ?>">

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/quemsomos/css/quemsomos.css')))); ?>
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-3">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="menu-4">
        <div class="col-md-12 menu">
            <div class="container">
                <div class="col-md-9 menu-items">
                    <div class="row">
                        <ul class="nav navbar-nav pull-left">
                            <li><a href="<?=base_url(); ?>" class="imobiliaria-link-logo"><img src="<?=base_url('assets/images/logo.png'); ?>" alt="logo" class="img-responsive img-imobiliaria"></a></li>
                            <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                            <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                            <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                            <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                            <? if($this->session->has_userdata('usuario')) : ?>
                                <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                            <? else : ?>
                                <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                            <?endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 pesquisa-imovel">
                    <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu-rapido">
            <div class="container">
                <div class="text-center menu-items col-xs-12">
                    <ul>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=2&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=&limite=12&ordenacao=valor+ASC')?>">CASAS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=1&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC')?>">APARTAMENTOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=9&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&limite=12&ordenacao=valor+ASC')?>">TERRENOS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="realizacoes-1">
    <div class="container">
        <h3 class="text-center">Imobiliária Macri</h3>
        <div class="col-md-6 sobre-empresa">
            <div class="row">
                <p>A Imobiliária Macri iniciou suas atividades em 22 de outubro de 2008, pelo proprietário Marcelo Lindemann Nunes. Atuando no setor de Compra, Venda, Aluguéis de Temporada, Avaliações e Regularizações de Imóveis. A Imobiliária Macri atua e conhece cada segmento do mercado imobiliário, com o objetivo de bem servir seus clientes, tornando-se a cada dia, uma empresa sólida e reconhecida pelo grau de profissionalismo e comprometimento nos serviços prestados. Faça uma visita a nossa Imobiliária, conheça nossa equipe, nossos conceitos inovadores e excelentes serviços imobiliários. Com a Imobiliária Macri, você tem a garantia de realizar ótimo negócio.</p>
            </div>
        </div>
        <div class="col-md-6 img">
            <img src="<?= base_url('assets/images/logo.png'); ?>" alt="" class="img-responsive center-block">
            <hr style="float: initial;">
        </div>
    </div>
</div><div class="col-md-12 destaques-imobiliaria-1">
    <div class="container">
        <div class="row">
            <div class="col-md-4 sobre-empresa">
                <h3>Visão</h3>
                <p>Ser a empresa referência em segurança nos negócios imobiliários.Trabalhar com foco na satisfação total de nossos clientes.Visar sempre a excelência de nossos serviços.Manter e reforçar a tradição e credibilidade conquistada em mais de 9 anos de mercado.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3>Missão</h3>
                <p>Ser referência de relacionamento, proporcionar felicidade e superando as expectativas de nossos clientes internos, externos e parceiros, por meio da excelência no atendimento e credibilidade nos negócios imobiliários.</p>
            </div>
            <div class="col-md-4 sobre-empresa">
                <h3 class="text-left">Valores</h3>
                <div class="row">
                    <div class="col-xs-6">
                        <p>Profissionalismo;
                            <br>
                            Parceria;
                            <br>
                            Inovação;
                            <br>
                            Transparência;
                            <br>
                            Excelência;
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>Adaptabilidade;
                            <br>
                            Credibilidade;
                            <br>
                            Ética;
                            <br>
                            Sustentabilidade;
                            <br>
                            Felicidade.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12"><hr style="width: 50%;"></div>
            <div class="col-md-6 sobre-empresa">
                <div class="row">
                    <div class="col-xs-4">
                        <a href="http://www8.caixa.gov.br/siopiinternet/simulaOperacaoInternet.do?method=inicializarCasoUso" target="_blank">
                            <img src="<?= base_url('assets/images/logo-caixa-financiamento.jpg'); ?>" class="img-responsive pull-left" alt="logo financiamento caixa" title="Financiamento Caixa" style="margin-top: 10px;">
                        </a>
                    </div>
                    <div class="col-md-8">
                        <p><h4 style="text-align: left; font-size: 17px;">Financiamento Imobiliário Caixa</h4>Atuando no setor de Compra, Venda, Aluguéis de Temporada, Avaliações, Regularizações de Imóveis e Financiamentos Bancários através da Caixa Econômica Federal, a qual a empresa atua como correspondente negocial, tem como mercado foco, Tramandaí, Imbé e seus balneários.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <p style="font-size: 50px; text-align: center; width: 80%; margin-left: auto; margin-right: auto; font-style: italic; margin-top: 60px; line-height: 60px; font-family: fantasy; text-shadow: 1px 1px rgba(255, 165, 0, 0.79); color: #174180;">Sua satisfação é nossa meta!</p>
            </div>
        </div>
    </div>
</div>
    <div class="col-md-12 sobre-socios-1">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="<?= base_url('assets/images/macri1.jpg'); ?>" alt="foto filial imbé" class="img-responsive" style="height:300px;">
                <h3>Imbé</h3>
                <? $imbe = $this->config->item('imobiliarias')['imbe']; ?>
                <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                <p><?= $imbe['rua'] . ' - ' . $imbe['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $imbe['email']; ?></strong><br><?= $imbe['telefones']; ?></p>
            </div>
            <div class="col-md-6">
                <img src="<?= base_url('assets/images/macri2.jpg'); ?>" alt="foto filial tramandaí" class="img-responsive" style="height:300px;">
                <h3>Tramandaí</h3>
                <? $tramandai = $this->config->item('imobiliarias')['tramandai']; ?>
                <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                <p><?= $tramandai['rua'] . ' - ' . $tramandai['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $tramandai['email']; ?></strong><br><?= $tramandai['telefones']; ?></p>
            </div>
        </div>
    </div>
</div>
<div class="footer-2 col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 logo-simples">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <? foreach ($this->config->item('imobiliarias') as $imobiliaria) : ?>
                <div class="col-md-3 col-xs-12">
                    <span><?= $imobiliaria['cidade']; ?></span>
                    <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                    <p><?= $imobiliaria['rua'] . ' - ' . $imobiliaria['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $imobiliaria['email']; ?></strong><br><?= $imobiliaria['telefones']; ?></p>
                </div>
            <? endforeach; ?>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimobiliariamacri&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 footer-2-direitos-simples">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/" target="_blank"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>

</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/quemsomos/js/quemsomos.js')))); ?>
</footer>
</html>