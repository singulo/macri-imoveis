<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.png'); ?>">
    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/condominio/detalhe/css/detalhe.css')))); ?>

    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-3">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="menu-4">
        <div class="col-md-12 menu">
            <div class="container">
                <div class="col-md-9 menu-items">
                    <div class="row">
                        <ul class="nav navbar-nav pull-left">
                            <li><a href="<?=base_url(); ?>" class="imobiliaria-link-logo"><img src="<?=base_url('assets/images/logo.png'); ?>" alt="logo" class="img-responsive img-imobiliaria"></a></li>
                            <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                            <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                            <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                            <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                            <? if($this->session->has_userdata('usuario')) : ?>
                                <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                            <? else : ?>
                                <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                            <?endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 pesquisa-imovel">
                    <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu-rapido">
            <div class="container">
                <div class="text-center menu-items col-xs-12">
                    <ul>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=2&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=&limite=12&ordenacao=valor+ASC')?>">CASAS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=1&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC')?>">APARTAMENTOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=9&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&limite=12&ordenacao=valor+ASC')?>">TERRENOS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
<? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>

<div class="detalhes-condominio-1">
    <div class="container condominio">
        <div class="col-md-12 nome-condominio">
            <span><?=$condominio->nome; ?></span>
        </div>
        <div class="col-xs-12 col-md-6 detalhes-condominio">
            <h4>Imagens do <?= $condominio->nome; ?></h4>
            <div class="row">
                <div class="col-md-12 imagem-principal">
                    <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->fotos_principais[0]->arquivo  ; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                    <? if ($condominio->lancamento == 1) : ?>
                        <div class="ribbon lancamento"><span>Lançamento</span></div>
                    <?endif; ?>
                </div>
            </div>
            <div class="col-md-12 medias-condominio">
                <div class="row">
                    <ul class="nav nav-tabs" role="tablist">
                        <? if(count($condominio->fotos['normais']) > 0) : ?>
                            <li role="presentation" class="active"><a href="#galeria-fotos" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-camera-retro"></i><span class="hidden-xs hidden-sm">Fotos</span><span> (<?= count($condominio->fotos['normais']);?>)</span></a></li>
                        <? endif; ?>
                        <? if(count($condominio->fotos['plantas']) > 0) : ?>
                            <li role="presentation"><a href="#galeria-plantas" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-crop"></i><span class="hidden-xs hidden-sm">Plantas</span><span> (<?= count($condominio->fotos['plantas']);?>)</span></a></li>
                        <? endif; ?>
                        <? if(count($condominio->videos) > 0) : ?>
                            <li role="presentation"><a href="#galeria-videos" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-play"></i><span class="hidden-xs hidden-sm">Videos</span><span> (<?= count($condominio->videos); ?>)</a></li>
                        <? endif; ?>
                    </ul>
                    <div class="tab-content">
                        <? if(count($condominio->fotos['normais']) > 0) : ?>
                            <div role="tabpanel" class="tab-pane active" id="galeria-fotos">
                                <? foreach($condominio->fotos['normais'] as $foto) : ?>
                                    <div class="item">
                                        <a href="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>">
                                            <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)">
                                                <i class="fa fa-expand" aria-hidden="true"></i>
                                            </div>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                        <? if(count($condominio->fotos['plantas']) > 0) : ?>
                            <div role="tabpanel" class="tab-pane" id="galeria-plantas" style="display: none;">
                                <? foreach($condominio->fotos['plantas'] as $foto) : ?>
                                    <div class="item">
                                        <a href="<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>">
                                            <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $foto->arquivo; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)">
                                                <i class="fa fa-expand" aria-hidden="true"></i>
                                            </div>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                        <? if(count($condominio->videos) > 0) : ?>
                            <div role="tabpanel" class="tab-pane" id="galeria-videos" style="display: none;">
                                <? foreach($condominio->videos as $video) : ?>
                                    <div class="item">
                                        <a href="https://www.youtube.com/watch?v=<?= $video->id_youtube; ?>">
                                            <div class="imagem" style="background-image: url(http://img.youtube.com/vi/<?= $video->id_youtube; ?>/0.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)">
                                                <i class="fa fa-play" aria-hidden="true"></i>
                                            </div>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="detalhes">
                <h4>Descrição</h4>
                <p><?= nl2br($condominio->descricao); ?></p>
            </div>
            <? if(count($condominio->complementos) > 0): ?>
                <div class="complementos row">
                    <div class="col-md-12">
                        <h4>Infraestrutura</h4>
                    </div>
                    <?foreach ($condominio->complementos as $complemento) :?>
                        <div class="col-md-4 col-xs-6 complemento">
                            <span><i class="fa fa-check"></i><?= $complemento->complemento?></span>
                        </div>
                    <?endforeach ; ?>
                </div>
            <?endif; ?>
        </div>
        <? if(count($condominio->imoveis) > 0): ?>
            <div class="col-md-6 imoveis-condominio">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Imóveis em <?= $condominio->nome; ?></h4>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div class="row">
                            <? $i = 1;  foreach ($condominio->imoveis as $imovel) :?>
                                <div class="col-md-6 col-xs-12 previa-imovel">
                                    <div class="row">
                                        <a class="imovel-url" href="<?= imovel_url($imovel); ?>">
                                            <div class="col-md-12 img-imovel">
                                                <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                                                <div class="ribbon <?= strtolower(Finalidades::toString($imovel->finalidade)); ?>"><span class="finalidade"><?= Finalidades::toString($imovel->finalidade); ?></span></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <span class="imovel-tipo pull-left"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></span>
                                                        <span class="valor-imovel"><?= format_valor_miniatura($imovel, 'R$ '); ?></span>
                                                    </div>
                                                </div>
                                                <span class="detalhes text-left"><?= $imovel->dormitorios . ' dorm'; ?><? if($imovel->area_total > 0) echo ' | ' . $imovel->area_total . 'm²'; ?><? if($imovel->garagem > 0) echo ' | ' . $imovel->garagem . ' ' . texto_para_plural_se_necessario($imovel->garagem, 'vaga'); ?> <? if($imovel->suites > 0) echo ' | ' . $imovel->suites . ' ' . texto_para_plural_se_necessario($imovel->suites, 'suíte'); ?></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?endif; ?>
    </div>
</div><div class="footer-2 col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 logo-simples">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <? foreach ($this->config->item('imobiliarias') as $imobiliaria) : ?>
                <div class="col-md-3 col-xs-12">
                    <span><?= $imobiliaria['cidade']; ?></span>
                    <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                    <p><?= $imobiliaria['rua'] . ' - ' . $imobiliaria['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $imobiliaria['email']; ?></strong><br><?= $imobiliaria['telefones']; ?></p>
                </div>
            <? endforeach; ?>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimobiliariamacri&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 footer-2-direitos-simples">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/" target="_blank"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>

</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/condominio/detalhe/js/detalhe.js')))); ?>
</footer>
</html>