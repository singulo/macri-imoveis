<!-- Bootstrap CSS -->
<link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

<!--Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!-- Bootstrap-select -->
<link href="<?= base_url('assets/plugins/bootstrap-select/css/bootstrap-select.min.css');?>" rel="stylesheet">

<!-- Font-Awesome -->
<link href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">

<!-- Bootstrap-slider -->
<link href="<?= base_url('assets/plugins/bootstrap-slider/css/bootstrap-slider.min.css');?>" rel="stylesheet">

<!-- Jquery-simplePagination-->
<link href="<?= base_url('assets/plugins/simplePagination/css/simplePagination.css');?>" rel="stylesheet">

<!-- Custom -->
<link href="<?= base_url('assets/css/macri.css');?>" rel="stylesheet">

<!-- Jquery -->
<script src="<?=base_url('assets/plugins/jquery/jquery-2.2.0.min.js')?>"></script>

<!-- Owl-carousel -->
<link href="<?= base_url('assets/plugins/owl-carousel/css/owl.carousel.min.css');?>" rel="stylesheet">

<!--Owl-carousel-->
<script src="<?=base_url('assets/plugins/owl-carousel/js/owl.carousel.js')?>"></script>

<!--Magnific-Popup-->
<link href="<?=base_url('assets/plugins/magnific-popup/css/magnific-popup.css')?>" rel="stylesheet">

<!--Alertify-->
<link href="<?=base_url('assets/plugins/alertify/css/alertify.css')?>" rel="stylesheet">


<? if (isset($appendStyle)) : ?>
    <? foreach ($appendStyle as $style) :?>
        <link rel="stylesheet" href="<?= $style; ?>">
    <?endforeach; ?>
<? endif; ?>

<link href="<?=base_url('assets/pages/pesquisa-rapida/css/pesquisa-rapida.css')?>" rel="stylesheet">

<link href="<?=base_url('assets/css/custom.css')?>" rel="stylesheet">

<!--Macri-->
<link href="<?=base_url('assets/css/macri.css')?>" rel="stylesheet">


<input id="base_url" type="hidden" value="<?= base_url(); ?>">
<input type="hidden" id="filial_fotos_imoveis" value="<?= $_SESSION['filial']['fotos_imoveis']; ?>">
<input id="filial_sessao" type="hidden" value="<? if(isset($_SESSION['filial']['chave'])) {echo  $_SESSION['filial']['chave'];}?>">
<input id="usuario" type="hidden" value='<?= json_encode($this->session->userdata('usuario')); ?>'>

