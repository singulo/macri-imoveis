<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Empreendimento_model.php');
require_once(__DIR__ . '/../libraries/Finalidades.php');

class Imovel_Model extends Empreendimento_Model
{
    protected $table = 'view_imovel';
    protected $tb_tipo = 'tb_imovel_tipo';
    protected $tb_foto = 'tb_imovel_foto';
    public $tb_foto_foreign_key = 'id_imovel';
    public $tb_video = 'tb_imovel_video';
    public $tb_video_foreign_key = 'id_imovel';
    protected $tb_empreendimento_complemento = 'tb_imovel_complemento';
    protected $tb_complemento_foreign_key = 'id_imovel';

    protected $filtro_sql_condition = array(
        'id_tipo'       => 'where_in',
//        'id_condominio' => 'where',
    );

    public function todos_complementos()
    {
        return $this->db
            ->order_by('tipo')
            ->order_by('complemento')
            ->get($this->tb_complemento)
            ->result();
    }

    public function cidades_imoveis()
    {
        return $this->db->query('SELECT distinct(cidade) FROM ' . $this->table . ' ORDER BY cidade ASC')->result();
    }

    public function tipos()
    {
        return $this->db
            ->select('id, tipo, mostrar_site')
            ->get($this->tb_tipo)
            ->result();
    }

    public function pesquisar(array $filtro, $pagina, $limite, $order_by = NULL)
    {
        $this->select_foto_principal();

        $this->monta_filtro($filtro);

        if(! is_null($order_by))
            $this->db->order_by($order_by);

        return $this->db->limit($limite, ($pagina * $limite))->get($this->table)->result();
    }

    public function pesquisar_total_resultados(array $filtro)
    {
        $this->monta_filtro($filtro);

        return $this->db->from($this->table)->count_all_results();
    }

    private function monta_filtro(array $filtro)
    {
        $valoresCond = array();
        if(isset($filtro['preco_min']) && $filtro['preco_min'] > 0)
            $valoresCond['valor >='] = (int)$filtro['preco_min'];
        if(isset($filtro['preco_max']) && $filtro['preco_max'] > 0 && $filtro['preco_max'] != 3000000)
            $valoresCond['valor <='] = (int)$filtro['preco_max'];

        if(isset($filtro['metragem_min']) && $filtro['metragem_min'] > 0)
            $valoresCond['area_total >='] = (int)$filtro['metragem_min'];
        if(isset($filtro['metragem_max']) && $filtro['metragem_max'] > 0 && $filtro['metragem_max'] != 300)
            $valoresCond['area_total <='] = (int)$filtro['metragem_max'];

        unset($filtro['preco_min']);
        unset($filtro['preco_max']);
        unset($filtro['metragem_min']);
        unset($filtro['metragem_max']);

        foreach($filtro as $key => $value)
        {
            if($value == '' || is_null($value))
                unset($filtro[$key]);
        }

        foreach ($filtro as $key => $val)
            if (is_numeric($key)) // only numbers, a point and an `e` like in 1.1e10
                unset($filtro[$key]);

        if(isset($filtro['complementos']))
        {
            if(is_array($filtro['complementos']))
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento IN (" . implode(', ', $filtro['complementos']) . ")");
            else
                $this->db->join($this->tb_empreendimento_complemento, "$this->tb_empreendimento_complemento.id_imovel = $this->table.id AND $this->tb_empreendimento_complemento.id_complemento = " . $filtro['complementos']);

            unset($filtro['complementos']);
        }

        if(count($valoresCond) > 0)
            $this->db->where($valoresCond);

        $pesqConfigs = $this->config->item('pesquisa');

        foreach ($filtro as $key => $value)
        {
            if(isset($this->filtro_sql_condition[$key]))
            {
                $function = $this->filtro_sql_condition[$key];
                $this->db->$function($key, $value);
            }
            else if(is_array($value))
                $this->db->where_in($key, $value);
            else if(isset($pesqConfigs[$key]) && $pesqConfigs[$key] == $value)
                $this->db->where($key . ' >=', $value);
            else
                $this->db->like($key, $value);
        }
    }

    /**
     * @param $imovel
     * @param $limit
     * @return mixed
     */
    public function similares($imovel, $limit, $foto_principal = true)
    {
        $id_ignorados = [];

        $similares = $this->buscar_similares($imovel, $limit, $foto_principal);

        if($limit - (count($similares)) > 0)
        {
            foreach( $similares as $imovel_similar)
            {
                $id_ignorados[] = $imovel_similar->id;
            }

            $similares = array_merge($similares, $this->buscar_similares($imovel, $limit - count($similares), $foto_principal, false, $id_ignorados ));
        }

        return $similares;
    }

    protected function valores_similares($imovel_valor, $porcentagem)
    {
        $valor = (($imovel_valor * $porcentagem) / 100);

        $valor_maximo = $imovel_valor + $valor;
        $valor_minimo = $imovel_valor - $valor;

        if($imovel_valor > 0)
        {
            $this->db->where('valor <=', $valor_maximo);

            if($valor_minimo > 0)
            {
                $this->db->where('valor >=', $valor_minimo);
            }
        }
    }

    public function pelo_condominio($id_condominio, $foto_principal = true)
    {
        if($foto_principal)
            $this->select_foto_principal();

        return $this->db
            ->where('id_condominio', $id_condominio)
            ->get($this->table)
            ->result();
    }

    public function destaques_com_lat_lng($limite = 10, $rand = true, $tipos = array(), $complementos = array())
    {
        $this->db->where('lat_lng !=', '');

        return $this->destaques($limite, $rand, $tipos, $complementos);
    }


    public function buscar_similares($imovel, $limit, $foto_principal, $do_condominio = true, $id_ignorados = [])
    {
        if($foto_principal)
            $this->select_foto_principal();

        $id_ignorados[] = $imovel->id;

        $this->db
            ->order_by('RAND()')
            ->where_not_in('id', $id_ignorados);

        if($do_condominio && $imovel->id_condominio > 0 )
            $this->db->where('id_condominio', $imovel->id_condominio);



        $config = $this->config->item('pesquisa');

        if( isset($config['similares_valores'][$imovel->finalidade]) )
        {
            $porcentagem = $config['similares_valores'][$imovel->finalidade];

            if($imovel->finalidade == Finalidades::Venda)
                $this->valores_similares($imovel->valor, $porcentagem);
            else if($imovel->finalidade == Finalidades::Aluguel)
                $this->valores_similares($imovel->valor_aluguel_mensal, $porcentagem);
            else
                $this->valores_similares($imovel->valor_aluguel_diario, $porcentagem);
        }

        return $this->db
            ->limit($limit)
            ->get($this->table)
            ->result();
    }

    public function pelo_seo_link($link)
    {
        return $this->obter($link, 'seo_link');
    }
}