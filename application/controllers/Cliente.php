<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "../modules/simples/controllers/Base_cliente_controller.php";

/**
 * @property Clientes_imoveis_log_model $clientes_imoveis_log_model
 **/
class Cliente extends Base_Cliente_Controller
{
    public function login()
    {
        $this->load->model('simples/clientes_model');
        $cliente = $this->clientes_model->pelo_email($_POST['email']);

//        if($cliente != null)
//        {
//            $this->load->model('simples/clientes_imoveis_log_model');
//            $_SESSION['quantidade_imoveis_visualizados'] = count($this->clientes_imoveis_log_model->imoveis_visualizados($_POST['email']));
//        }

        parent::login();
    }

    public function logout()
    {
        $_SESSION['quantidade_imoveis_visualizados'] = 0;
        parent::logout();
    }
}