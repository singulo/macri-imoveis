<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>
<? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <? $imovel_titulo = $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo . ' ' . ($imovel->dormitorios > 0 ? $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório') : '') . ' ' . $imovel->cidade; ?>

    <title><?= $imovel_titulo; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.png'); ?>">

    <? if (isset($imovel)) : ?>
        <meta property="og:title" content="<?= $imovel_titulo . ' - ' . $_SESSION['filial']['nome']; ?>">
        <meta property="og:site_name" content="<?= $_SESSION['filial']['nome']; ?>">
        <meta property="og:image" content="<?=  $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>">
        <meta property="og:description" content="<?=  substr($imovel->descricao,0,152)."..."?>">
    <?endif;?>

    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/imovel/css/detalhe.css')))); ?>

    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body class="pagina-imovel-detalhe">
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-3">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="menu-4">
        <div class="col-md-12 menu">
            <div class="container">
                <div class="col-md-9 menu-items">
                    <div class="row">
                        <ul class="nav navbar-nav pull-left">
                            <li><a href="<?=base_url(); ?>" class="imobiliaria-link-logo"><img src="<?=base_url('assets/images/logo.png'); ?>" alt="logo" class="img-responsive img-imobiliaria"></a></li>
                            <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                            <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                            <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                            <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                            <? if($this->session->has_userdata('usuario')) : ?>
                                <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                            <? else : ?>
                                <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                            <?endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 pesquisa-imovel">
                    <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu-rapido">
            <div class="container">
                <div class="text-center menu-items col-xs-12">
                    <ul>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=2&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=&limite=12&ordenacao=valor+ASC')?>">CASAS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=1&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC')?>">APARTAMENTOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=9&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&limite=12&ordenacao=valor+ASC')?>">TERRENOS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="detalhes-imovel-1">
    <div class="container">
        <div class="col-md-6">
            <div class="row imagens-imovel">
                <div class="img-destaque">
                    <? if(isset($imovel->fotos_principais[0])) : ?>
                        <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>">
                            <img src="<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->fotos_principais[0]->arquivo; ?>" alt="" class="img-responsive">
                            <div class="ribbon <?= strtolower(Finalidades::toString($imovel->finalidade)); ?>"><span><?= Finalidades::toString($imovel->finalidade); ?></span></div>
                        </a>
                    <?else :?>
                        <div class="ribbon <?= strtolower(Finalidades::toString($imovel->finalidade)); ?>"><span><?= Finalidades::toString($imovel->finalidade); ?></span></div>
                        <img src="<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>" alt="">
                    <?endif; ?>
                </div>
                <? if($this->session->has_userdata('usuario')) : ?>
                    <div class="img-previa">
                        <? foreach($imovel->fotos['normais'] as $foto) : ?>
                            <div class="item">
                                <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>">
                                    <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                <? endif; ?>
            </div>
            <? if(!$this->session->has_userdata('usuario')) : ?>
                <div class="aviso-login">
                    <div class="row">
                        <? $total_midias = $total_midias_disponiveis->fotos + $total_midias_disponiveis->videos; ?>
                        <div class="hidden-xs hidden-sm">
                            <p class="pull-left"><i class="fa fa-camera" aria-hidden="true"></i> Efetue o login para ver todas as <? if($total_midias > 0) echo '<b>' . $total_midias . '</b> '; ?>mídias deste imóvel.</p>
                            <a href="#modal-login" data-toggle="modal" class="btn btn-success pull-right">Logar</a>
                        </div>
                        <div class="visible-xs visible-sm">
                            <p class="pull-left"><i class="fa fa-camera" aria-hidden="true"></i> Efetue o login para ver todas as <? if($total_midias > 0) echo '<b>' . $total_midias . '</b> '; ?>mídias deste imóvel.</p>
                            <a href="#modal-login" data-toggle="modal" class="btn btn-success col-xs-12">Logar</a>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
        <div class="col-xs-12 col-md-6 descricao-imovel">
            <div class="col-md-12 tipo-imovel">
                <hr class="hidden-sm hidden-xs">
                <? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>
                <h1><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?><? if($imovel->dormitorios > 0) echo ' ' . $imovel->dormitorios . ' ' . texto_para_plural_se_necessario($imovel->dormitorios, 'dormitório'); ?></h1>
                <hr class="hidden-sm hidden-xs">
                <h3><?=$imovel->cidade; ?> / <?=$imovel->bairro; ?></h3>
            </div>
            <div class="col-md-12 descricao">
                <p><?= nl2br($imovel->descricao); ?></p>
            </div>
            <div class="col-md-12 infra-imovel">
                <? if( $imovel->dormitorios > 0) : ?>
                    <i class="fa fa-bed"><br><span><?=$imovel->dormitorios; ?> dorm. </span></i>
                <? endif; ?>
                <? if( $imovel->suites > 0) : ?>
                    <i class="fa fa-bathtub"><br><span><?=$imovel->suites; ?> suites. </span></i>
                <? endif; ?>
                <? if( $imovel->garagem > 0) : ?>
                    <i class="fa fa-car"><br><span><?=$imovel->garagem; ?> vagas. </span></i>
                <? endif; ?>
                <? if( $imovel->area_util > 0) : ?>
                    <i class="fa fa-arrows-alt"><br><span><?=$imovel->area_util; ?>m². </span></i>
                <? endif; ?>
            </div>
            <div class="col-xs-12">
                <span class="titulo-descicao">DETALHES DO IMÓVEL<small> cód: <?=$imovel->id; ?></small></span>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 valor-imovel no-padding-left">
                    <? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
                    <span><small>R$</small> <?= format_valor($imovel->valor); ?></span>
                </div>
                <div class="col-md-6 agendar-visita no-padding-right">
                    <div class="row">
                        <button type="button" class="btn-interesse"><i class="fa fa-paper-plane-o"></i> AGENDAR VISITA</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><div class="complementos-imovel-1">
    <div class="container">
        <? if(isset($imovel->complementos) && count($imovel->complementos) > 0) : ?>
            <div class="col-md-6 complementos">
                <p class="titulo">Mais detalhes deste imóvel</p>
                <? foreach($imovel->complementos as $complemento) :?>
                    <div class="col-md-3 col-xs-6">
                        <div class="row text-left">
                            <span class="fa fa-check"></span> <small><em><?= $complemento->complemento; ?></em></small>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endif; ?>
        <? if(isset($imovel->condominio) && count($imovel->condominio->complementos) > 0) : ?>
            <div class="col-xs-12 col-md-6 complementos-condominio">
                <p class="titulo">Infraestrutura do condomínio</p>
                <? foreach($imovel->condominio->complementos as $complemento) :?>
                    <div class="col-md-3 col-xs-6">
                        <div class="row text-left">
                            <span class="fa fa-check"></span> <small><em><?= $complemento->complemento; ?></em></small>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endif; ?>
    </div>
</div><? if($this->session->has_userdata('usuario')) : ?>
    <div class="col-md-12 medias-1">
        <div class="container">
            <div class="row">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab-fotos" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-camera-retro"></i><span>Fotos (<?= count($imovel->fotos['normais']);?>)</span></a></li>
                    <? if(count($imovel->fotos['plantas']) > 0) : ?>
                        <li role="presentation"><a href="#tab-plantas" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-pencil"></i><span>Plantas (<?= count($imovel->fotos['plantas']);?>)</span></a></li>
                    <? endif; ?>
                    <? if(count($imovel->videos) > 0) : ?>
                        <li role="presentation"><a href="#tab-videos" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-video-camera"></i><span>Videos (<?= count($imovel->videos); ?>)</span></a></li>
                    <? endif; ?>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="tab-fotos">
                        <div class="col-md-12 conteudo-panel">
                            <div class="row conteudo-medias center-block">
                                <? foreach($imovel->fotos['normais'] as $foto) : ?>
                                    <div class="col-md-2 margin-foto">
                                        <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" class="img-imovel"  title="<?= $foto->legenda; ?>">
                                            <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo;?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                                        </a>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <? if(count($imovel->fotos['plantas']) > 0) : ?>
                        <div role="tabpanel" class="tab-pane fade" id="tab-plantas">
                            <div class="col-md-12 conteudo-panel">
                                <div class=" row conteudo-medias ">
                                    <? foreach($imovel->fotos['plantas'] as $foto) : ?>
                                        <div class="col-md-2 margin-foto">
                                            <a href="<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo; ?>" class="img-imovel"  title="<?= $foto->legenda; ?>">
                                                <div class="img" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $foto->arquivo;?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>"></div>
                                            </a>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                    <? if(count($imovel->videos) > 0) : ?>
                        <div role="tabpanel" class="tab-pane fade" id="tab-videos">
                            <? foreach($imovel->videos as $video) : ?>
                                <div class="col-md-4">
                                    <div class="img-previa-video">
                                        <a class="video-youtube" href="https://www.youtube.com/watch?v=<?= $video->id_youtube; ?>">
                                            <img class="img-responsive" src="//img.youtube.com/vi/<?= $video->id_youtube; ?>/0.jpg" alt="">
                                            <span class="fa fa-play"></span>
                                        </a>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
<? endif; ?><div class="sugestoes-1">
    <div class="container">
        <span class="titulo">Mais alguns imóveis que separamos para você</span><br>
        <? for($i = 0; $i <= 2; $i++ ) : ?>
            <? if(isset($imoveis_similares[$i])) :?>
                <div class="col-md-4">
                    <div class="col-md-12 sugestao-imovel" style="background-image: url(<?=$_SESSION['filial']['fotos_imoveis']. $imoveis_similares[$i]->foto; ?>)">
                        <div class="row">
                            <a href="<?= imovel_url($imoveis_similares[$i]); ?>">
                                <? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
                                <div class="col-md-12 background-sugestao">
                                    <span class="tipo-imovel"><?= $_SESSION['filial']['tipos_imoveis'][$imoveis_similares[$i]->id_tipo]->tipo ?></span><br>
                                    <span class="detalhes"><?= $imoveis_similares[$i]->dormitorios; ?> dormitorios<? if($imoveis_similares[$i]->area_util > 0) echo ' | ' . $imoveis_similares[$i]->area_util . 'm²'; ?></span><br>
                                    <hr>
                                    <span><?=format_valor($imoveis_similares[$i]->valor,'R$'); ?></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            <?endif; ?>
        <? endfor; ?>
    </div>
</div>

<? $this->load->view('templates/pesquisa'); ?>

<div class="footer-2 col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 logo-simples">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <? foreach ($this->config->item('imobiliarias') as $imobiliaria) : ?>
                <div class="col-md-3 col-xs-12">
                    <span><?= $imobiliaria['cidade']; ?></span>
                    <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                    <p><?= $imobiliaria['rua'] . ' - ' . $imobiliaria['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $imobiliaria['email']; ?></strong><br><?= $imobiliaria['telefones']; ?></p>
                </div>
            <? endforeach; ?>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimobiliariamacri&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 footer-2-direitos-simples">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/" target="_blank"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>

</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/imovel/js/detalhe.js')))); ?>
</footer>
</html>