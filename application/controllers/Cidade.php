<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "../modules/simples/controllers/Base_cidade_controller.php";

/**
 * @property Bairro_Model $bairro_model
 */
class Cidade extends Base_Cidade_Controller
{
    public function obter_cidade_pela_uf()
    {
        $data = parent::obter_cidade_pela_uf();
        $data['status'] = true;

        echo json_encode($data);
    }

    public function obter_bairros_pela_cidade()
    {
        $this->load->model('bairro_model');

        $data['bairros'] = $this->bairro_model->pela_cidade_dos_imoveis($_POST['cidade']);
        $data['status'] = true;

        echo json_encode($data);
    }
}