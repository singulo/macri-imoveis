/*
* Ajustes no layout a pedido do cliente
*/

/**
 * Por não trabalhar com aluguel está opção foi removida das pesquisas
 */
$('select[name=finalidade] option[value=2]').remove();

/**
 * Carrega bairros no filtro de acordo com a cidade
 */

console.log($('.pesquisa-avancada-1 #filtro-imovel select.selectpicker[name="cidade"]').val());
console.log($('.pesquisa-avancada-1 #filtro-imovel select.selectpicker[name="bairro"]').val());

var ignorado_primeiro_on_change_cidade = false;
function obter_bairros(form)
{
    var select_bairro = $($(form).find('select[name="bairro"]')[0]);

    if(document.location.pathname == "/imovel/pesquisar")
    {
        if( ! ignorado_primeiro_on_change_cidade)
        {
            ignorado_primeiro_on_change_cidade = true;

            if(filtro != undefined && filtro.filtro != undefined && filtro.filtro.bairro != undefined)
            {
                select_bairro.prop('disabled', true);

                obter_bairros_pela_cidade( { cidade: $(form).find('select[name="cidade"]').val()[0] }, {
                    successCallback: function(data)
                    {
                        select_bairro.find('option').remove();

                        $.each(data.bairros, function(index, bairro){
                            select_bairro.append($('<option>', {
                                value: bairro.bairro,
                                text: bairro.bairro
                            }));
                        });

                        select_bairro.val(filtro.filtro.bairro);

                        select_bairro.prop('disabled', false);

                        select_bairro.selectpicker('refresh');
                    },
                    failureCallback: function(data){

                    },
                    errorCallback: function(){

                    }
                })
            }
        }
    }
    else
    {
        select_bairro.find('option:not(.bs-title-option)').remove();
        select_bairro.prop('disabled', true);
        select_bairro.selectpicker('refresh');

        $(select_bairro.closest('.bootstrap-select')).find('.filter-option').text('Carregando...');

        if($(form).find('select[name="cidade"]').val() != null)
        {
            obter_bairros_pela_cidade( { cidade: $(form).find('select[name="cidade"]').val()[0] }, {
                successCallback: function(data)
                {
                    $.each(data.bairros, function(index, bairro){
                        select_bairro.append($('<option>', {
                            value: bairro.bairro,
                            text: bairro.bairro
                        }));
                    });

                    select_bairro.prop('disabled', false);

                    select_bairro.selectpicker('refresh');

                    $(select_bairro.closest('.bootstrap-select')).closest('.bootstrap-select').find('.filter-option').text('Selecione');
                },
                failureCallback: function(data){
                    alertify.error('Ocorreu um erro ao carregar os bairros. Por favor tente mais tarde!');
                },
                errorCallback: function(){
                    alertify.error('Ocorreu um erro ao carregar os bairros.');
                }
            })
        }
        else
            select_bairro.selectpicker('refresh');
    }
}

function obter_bairros_pela_cidade(cidade, simpleRequest)
{
    ajaxPost(
        cidade,
        $('#base_url').val() + 'cidade/obter_bairros_pela_cidade',
        simpleRequest
    );
}