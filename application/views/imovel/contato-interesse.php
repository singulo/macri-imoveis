<!-- Modal -->
<div class="modal fade" id="modal-contato-interesse" tabindex="-1" role="dialog" aria-labelledby="contatoInteresseModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Solicitar nosso contato</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="form-interesse" onsubmit="cliente_enviar_contato_interesse(); return false;">
                        <div class="form-group col-xs-12">
                            <input type="hidden" value="<?= $imovel->id; ?>" name="cod_imovel">
                            <small>Solicito informações sobre o imóvel código. <?= $imovel->id; ?>.</small>
                            <textarea class="form-control" name="obs"  style="resize: vertical;" placeholder="Se desejar, descreva suas dúvidas ou agurade contato"></textarea>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group col-xs-12">
                    <button type="button" onclick="cliente_enviar_contato_interesse();" data-loading-text="Aguarde..." class="btn btn-info btn-enviar" autocomplete="off">Enviar</button>
                </div>
            </div>
        </div>
    </div>
</div>