<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.png'); ?>">
    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/home/css/home.css')))); ?>

    <meta property="og:title" content="<?= $_SESSION['filial']['nome']; ?>">
    <meta property="og:site_name" content="<?= $_SESSION['filial']['nome']; ?>">
    <meta property="og:url" content="<?= $_SESSION['filial']['link']; ?>">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?= base_url('assets/images/logo-facebook.jpg'); ?>">
    <meta property="og:description" content="Atuando no setor de Compra, Venda, Aluguéis de Temporada, Avaliações e Regularizações de Imóveis. A Imobiliária Macri atua e conhece cada segmento do mercado imobiliário, com o objetivo de bem servir seus clientes, tornando-se a cada dia, uma empresa sólida e reconhecida pelo grau de profissionalismo e comprometimento nos serviços prestados.">

    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-3">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="menu-4">
        <div class="col-md-12 menu">
            <div class="container">
                <div class="col-md-9 menu-items">
                    <div class="row">
                        <ul class="nav navbar-nav pull-left">
                            <li><a href="<?=base_url(); ?>" class="imobiliaria-link-logo"><img src="<?=base_url('assets/images/logo.png'); ?>" alt="logo" class="img-responsive img-imobiliaria"></a></li>
                            <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                            <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                            <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                            <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                            <? if($this->session->has_userdata('usuario')) : ?>
                                <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                            <? else : ?>
                                <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                            <?endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 pesquisa-imovel">
                    <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu-rapido">
            <div class="container">
                <div class="text-center menu-items col-xs-12">
                    <ul>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=2&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=&limite=12&ordenacao=valor+ASC')?>">CASAS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=1&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC')?>">APARTAMENTOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=9&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&limite=12&ordenacao=valor+ASC')?>">TERRENOS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="banner-2 visible-md visible-lg">
    <div class="banner">
        <? foreach($_SESSION['filial']['banners'] as $banner) : ?>
            <div class="item" style="background-image: url(<?= $_SESSION['filial']['banners_uri'] . $banner->id . '.jpg'; ?>)">
                <? if (isset($banner->imovel)) : ?>
                    <div class="col-md-12 background-banner"></div>
                    <div class="container">
                        <div class="col-md-8 titulo-banner">
                            <div class="col-md-12">
                                <small><?= $banner->mensagem; ?></small>
                                <br>
                                <span><?= $_SESSION['filial']['tipos_imoveis'][$banner->imovel->id_tipo]->tipo; ?></span>
                            </div>
                            <div class="col-md-3 detalhes">
                                <a href="<?= imovel_url($banner->imovel); ?>">
                                    <span>Veja mais</span>
                                </a>
                            </div>
                        </div>
                    </div>
                <?else :?>
                    <div class="col-md-12 background-banner"></div>
                    <div class="container">
                        <div class="col-md-8 titulo-banner">
                            <div class="col-md-12">
                                <span><?= $banner->mensagem; ?></span>
                            </div>
                        </div>
                    </div>
                <?endif; ?>
            </div>
        <?endforeach; ?>
    </div>
</div>
<? $this->load->view('templates/pesquisa'); ?>
<?= $this->load->view('templates/pesquisa-rapida'); ?>

<? require_once MODULESPATH . 'simples/helpers/valor_imovel_formater_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>

<div class="col-md-12">
    <div class="container">
        <div class="row destaques-1">
            <div class="col-md-3 linha">
                <hr>
            </div>
            <div class="col-md-6 titulo-destaque">
                <span >Confira alguns de nossos destaques</span>
            </div>
            <div class="col-md-3 linha">
                <hr>
            </div>
            <? require_once MODULESPATH . 'simples/helpers/texto_helper.php'; ?>
            <? $count = 1; ?>
            <? foreach($destaques as $imovel) : ?>
                <div class="col-md-4 previa-imovel">
                    <a href="<?= imovel_url($imovel); ?>">
                        <div class="col-md-12 img-imovel" style="background-image: url(<?= $_SESSION['filial']['fotos_imoveis'] . $imovel->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)"></div>
                        <div class="col-md-12 detalhes-imovel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <span class="imovel-tipo pull-left"><?= $_SESSION['filial']['tipos_imoveis'][$imovel->id_tipo]->tipo; ?></span>
                                        <span class="valor-imovel"><?= format_valor_miniatura($imovel, 'R$ '); ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <? $detalhes = [];

                                            if($imovel->dormitorios > 0)
                                                $detalhes[] = $imovel->dormitorios . ' dorm.';
                                            if($imovel->area_total > 0)
                                                $detalhes[] = $imovel->area_total . 'm²';
                                            if($imovel->garagem > 0)
                                                $detalhes[] =  $imovel->garagem . ' ' . texto_para_plural_se_necessario($imovel->garagem, 'vaga');
                                            if($imovel->suites > 0)
                                                $detalhes[] = $imovel->suites . ' ' . texto_para_plural_se_necessario($imovel->suites, 'suíte');
                                        ?>
                                        <span class="detalhes pull-left"><?= join(' | ', $detalhes); ?></span>
                                        <span class="codigo pull-right">cód: <?= $imovel->id; ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <? if(($count % 3) == 0 && ( 6 != $count)) :?>

                    <div class="col-md-12 hidden-sm hidden-xs" style="height: 30px; margin-bottom: 16px;">
                        <hr style="width: 964px;">
                    </div>

                <?endif; ?>
                <?$count++; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>
<div class="col-md-12 condominios-1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 titulo">
                <span>Condomínios</span>
            </div>
            <div class="col-md-2">
                <hr>
            </div>
            <div class="col-md-12 logos-condominios">
                <?foreach ($_SESSION['filial']['condominios'] as $condominio)  :?>
                    <div class="item">
                        <a href="<?= base_url_filial('condominio?id='.$condominio->id )?>">
                            <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)"></div>
                        </a>
                    </div>
                <?endforeach; ?>
            </div>
        </div>
    </div>
</div><div class="footer-2 col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 logo-simples">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <? foreach ($this->config->item('imobiliarias') as $imobiliaria) : ?>
                <div class="col-md-3 col-xs-12">
                    <span><?= $imobiliaria['cidade']; ?></span>
                    <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                    <p><?= $imobiliaria['rua'] . ' - ' . $imobiliaria['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $imobiliaria['email']; ?></strong><br><?= $imobiliaria['telefones']; ?></p>
                </div>
            <? endforeach; ?>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimobiliariamacri&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 footer-2-direitos-simples">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/" target="_blank"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>

</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/home/js/home.js')))); ?>

    <script>
       /* $(window).load(function () {
            // retrieved this line of code from http://dimsemenov.com/plugins/magnific-popup/documentation.html#api
            $.magnificPopup.open({
                items: {
                    src: $('#base_url').val() + 'assets/images/locacao.jpeg'
                },
                type: 'image'

                // You may add options here, they're exactly the same as for $.fn.magnificPopup call
                // Note that some settings that rely on click event (like disableOn or midClick) will not work here
            }, 0);
        });*/
    </script>
</footer>
</html>