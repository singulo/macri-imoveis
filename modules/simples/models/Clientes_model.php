﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

/**
 * @property CI_DB_mysql_driver $db
 */
class Clientes_Model extends Base_Model {

	public $id;
	public $nome;
	public $email;
	public $email_alternativo;
	public $telefone_1;
	public $telefone_2;
	public $telefone_3;
	public $uf;
	public $cidade;
	public $bloqueado;
	public $excluido;
	public $id_corretor;
	public $cadastrado_em;
	public $ultimo_acesso;
	public $atualizado_em;
	public $atualizado_por;


	protected $table = 'tb_clientes';

	public function pelo_email($email)
	{
		return $this->db
			->where('email', $email)
			->get($this->table)->first_row();
	}

	public function editar(Clientes_Model $cliente, $registrar_copia = true)
	{
		//COPIA REGISTRO ANTES DA EDIÇÃO E SALVA NA TABELA DE AUDIÇÃO
		$cliente_copia = $this->obter($cliente->id);

		unset($cliente->table);
		unset($cliente->db);
		$cliente = array_filter((array)$cliente, function($val) { return ($val || is_numeric($val));});

		$this->db->where('id', $cliente['id']);
		$this->db->update($this->table, $cliente);
		$linhas_afetadas = $this->db->affected_rows();

		if($linhas_afetadas > 0 && $registrar_copia)
			$this->db->insert('tb_clientes_historico', $cliente_copia);

		return $linhas_afetadas;
	}

	public function registrar_ultimo_acesso($id, $data = NULL)
	{
		if(is_null($data))
			$data = date('Y-m-d H:i:s');

		$this->db
				->set('ultimo_acesso', $data)
					->where('id', $id)
						->update($this->table);

		return $this->db->affected_rows();
	}
}