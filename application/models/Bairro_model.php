<?php

class Bairro_Model extends CI_Model
{
    function pela_cidade_dos_imoveis($cidade)
    {
        return $this->db->select('DISTINCT(bairro)')
                    ->where('cidade', $cidade)
                        ->order_by('bairro')
                            ->get('tb_imovel')
                                ->result();
    }
}