<? require_once MODULESPATH . 'simples/helpers/simples_helper.php'; ?>

<!doctype html>
<html lang="pt-BR">

<head>

    <title><?= $_SESSION['filial']['nome']; ?></title>
    <meta name="discription" content=""> <!--- Discrição do site !-->
    <meta name="keywords" content=""> <!--- palavras chave !-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="viewport" content="width=device-width, user-scalable=no" />

    <link rel="shortcut icon" href="<?=base_url('assets/images/favicon.png'); ?>">
    <? $this->load->view('templates/styles', array('appendStyle' =>array(base_url('assets/pages/contato/css/contato.css')))); ?>
    <!--    <link rel="icon" type="image/png" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--" sizes="192x192">-->
    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

    <!--    <link rel="apple-touch-icon" sizes="180x180" href="--><?//=base_url('assets/images/logo-branco.png'); ?><!--">-->

</head>
<body>
<? $this->load->view('templates/modais-cliente'); ?>

<div class="visible-sm visible-xs menu-mobile-3">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= base_url_filial('home'); ?>"><img src="<?=base_url_filial('assets/images/logo.png', false); ?>" alt="logo-imobiliaria" class="logo-imobiliaria"></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-mobile">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pesquisa-toggle" data-toggle="collapse" data-target="#pesquisa-mobile">
                    <span class="fa fa-search"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="menu-mobile">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url_filial('quem-somos')?>">QUEM SOMOS</a></li>
                    <li><a href="<?=base_url_filial('imovel/pesquisar')?>">IMÓVEIS</a></li>
                    <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                    <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                    <? if($this->session->has_userdata('usuario')) : ?>
                        <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                    <? else : ?>
                        <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                    <?endif; ?>
                </ul>
            </div>
            <? $this->load->view('templates/pesquisa-mobile-1');?>
        </div>
    </nav>
</div>

<div class="visible-md visible-lg menu-desktop">
    <div class="menu-4">
        <div class="col-md-12 menu">
            <div class="container">
                <div class="col-md-9 menu-items">
                    <div class="row">
                        <ul class="nav navbar-nav pull-left">
                            <li><a href="<?=base_url(); ?>" class="imobiliaria-link-logo"><img src="<?=base_url('assets/images/logo.png'); ?>" alt="logo" class="img-responsive img-imobiliaria"></a></li>
                            <li><a href="<?=base_url('quem-somos')?>">QUEM SOMOS</a></li>
                            <li><a href="<?=base_url('imovel/pesquisar')?>">IMÓVEIS</a></li>
                            <li><a href="<?= base_url('condominio/lista')?>">EMPREENDIMENTOS</a></li>
                            <li><a href="<?=base_url_filial('contato')?>">CONTATO</a></li>
                            <? if($this->session->has_userdata('usuario')) : ?>
                                <li><a href="#modal-dados" data-toggle="modal"><?= strtoupper($this->session->userdata('usuario')->nome); ?></a></li>
                            <? else : ?>
                                <li><a href="#modal-login" data-toggle="modal">ENTRAR</a></li>
                            <?endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 pesquisa-imovel">
                    <form class="row" action="<?= base_url_filial('imovel/pesquisar'); ?>" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" placeholder="CÓDIGO DO IMÓVEL">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu-rapido">
            <div class="container">
                <div class="text-center menu-items col-xs-12">
                    <ul>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=2&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=&limite=12&ordenacao=valor+ASC')?>">CASAS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=1&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC')?>">APARTAMENTOS</a></li>
                        <li><a href="<?=base_url('imovel/pesquisar?id_tipo=9&dormitorios=&banheiros=&suites=&garagem=&preco_min=0&preco_max=3000000&metragem_min=0&metragem_max=300&id=&pagina=0&limite=12&ordenacao=valor+ASC')?>">TERRENOS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 mapa-1">
    <div id="map-canvas" data-lat-lng="<?= $_SESSION['filial']['latitude_longitude']; ?>" data-marker-none="true" class="mapa"></div>
</div><div class="form-contato-1">
    <div class="container">
        <div class="col-xs-12">
            <h3 class="text-center">Fale conosco</h3>
        </div>
        <div class="col-md-offset-3 col-md-6">
            <p class="text-center">Olá tudo bem? Queremos ouvir você! Este espaço serve para você tirar dúvidas ou nos deixar sua sugestão. Como podemos ajuda-lo? </p>
            <form id="form-contato" class="form-contato">
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="selectpicker" name="assunto" data-width="100%" title="ASSUNTO" required>
                            <option>Quero vender um imóvel</option>
                            <option>Quero comprar um imóvel</option>
                            <option>Dúvida</option>
                            <option>Críticas</option>
                            <option>Sugestões</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="nome" value="<? if( isset($this->session->userdata('usuario')->nome)) : echo $this->session->userdata('usuario')->nome; endif; ?>" required minlength="5" placeholder="SEU NOME">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="email" class="form-control" required name="email" value="<? if( isset($this->session->userdata('usuario')->email)) : echo $this->session->userdata('usuario')->email; endif; ?>" placeholder="EMAIL">
                    </div>
                    <div class="form-group">
                        <input type="text" name="telefone" required minlength="13" class="form-control telefone" value="<? if( isset($this->session->userdata('usuario')->telefone)) : echo $this->session->userdata('usuario')->telefone; endif; ?>" placeholder="TELEFONE">
                    </div>
                </div>
                <div class="col-md-12">
                    <textarea name="mensagem" class="form-control" required minlength="5" cols="10" rows="5" placeholder="Aqui você pode explicar melhor o motivo de seu contato, para que assim a pessoa ou setor responsável possa te retornar."></textarea>
                </div>
                <button class="btn-default btn-enviar" type="button" data-loading-text="Aguarde..." onclick="cliente_enviar_novo_contato();">Enviar <i class="fa fa-chevron-right"></i></button>
            </form>
        </div>
    </div>
</div><div class="col-md-12 condominios-1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 titulo">
                <span>Condomínios</span>
            </div>
            <div class="col-md-2">
                <hr>
            </div>
            <div class="col-md-12 logos-condominios">
                <?foreach ($_SESSION['filial']['condominios'] as $condominio)  :?>
                    <div class="item">
                        <a href="<?= base_url_filial('condominio?id='.$condominio->id )?>">
                            <div class="imagem" style="background-image: url(<?= $_SESSION['filial']['fotos_condominios'] . $condominio->foto; ?>), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>)"></div>
                        </a>
                    </div>
                <?endforeach; ?>
            </div>
        </div>
    </div>
</div><div class="footer-2 col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 logo-simples">
                <img src="<?= base_url_filial('assets/images/logo.png', false) ?>" alt="logo" class="img-responsive">
            </div>
            <? foreach ($this->config->item('imobiliarias') as $imobiliaria) : ?>
                <div class="col-md-3 col-xs-12">
                    <span><?= $imobiliaria['cidade']; ?></span>
                    <hr style="width: 50%; margin-left: 0; margin-top: 10px; margin-bottom: 10px;">
                    <p><?= $imobiliaria['rua'] . ' - ' . $imobiliaria['bairro']; ?><br><strong style="font-size: 1.35rem;"><?= $imobiliaria['email']; ?></strong><br><?= $imobiliaria['telefones']; ?></p>
                </div>
            <? endforeach; ?>
            <div class="col-md-4 facebook visible-lg visible-md">
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fimobiliariamacri&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="100%" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 footer-2-direitos-simples">
    <span class="center-block visible-lg visible-md"><a href="http://www.simplesimob.com.br/" target="_blank"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a> Desenvolvido por Simples Imob, todos os direitos reservados.</span>
    <span class="center-block visible-sm visible-xs"><a href="http://www.simplesimob.com.br/"><img src="<?= base_url_filial('assets/images/logo-simples.png', false) ?>" alt="Simples Imob - Sistema de Gerenciamento de Imobiliárias"></a></span>
</div>

</body>
<footer>
    <? $this->load->view('templates/scripts', array('appendScripts' =>array(base_url('assets/pages/contato/js/contato.js')))); ?>
</footer>
</html>