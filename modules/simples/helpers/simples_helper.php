<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function base_url_filial($acrescentar = '', $setar_filial = true)
{
    if(! $setar_filial)
        return base_url($acrescentar);

    $filial_acrescentar = 'filial=' . $_SESSION['filial']['chave'];

    if(strpos($acrescentar, '?') !== false)
        $filial_acrescentar = $acrescentar . '&' . $filial_acrescentar;
    else
        $filial_acrescentar = $acrescentar . '?' . $filial_acrescentar;

    return base_url($filial_acrescentar);
}

function imovel_url($imovel)
{
    return base_url_filial(( ! is_null($imovel->seo_link) && strlen($imovel->seo_link) > 0) ?
        $imovel->seo_link : 'imovel?id=' . $imovel->id);
}
