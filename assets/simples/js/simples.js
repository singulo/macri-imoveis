//EXIBE NOTIFICAÇÔES A DIREITA
alertify.logPosition("bottom right");

//TROCA MENSAGEM DA VALIDAÇÂO
$.validator.messages.required = 'Campo requerido!';
$.validator.messages.require_from_group = 'Por favor preencha pelo menos um telefone.';
$.validator.messages.equalTo = 'Os campos de senha devem ser iguais!';
$.validator.messages.minlength = 'Por favor preencha com no mínimo {0} characters.';
$.validator.messages.email = 'Por favor digite um email valido.';

$.simpleRequest = function(options){
    // This is the easiest way to have default options.
    return $.extend({
        // These are the defaults.
        successCallback: function(data){},
        failureCallback: function(data){},
        errorCallback: function(jqXhr, textStatus, errorThrown){},
        completeCallback: function(){}
    }, options );
};

function ajaxPost(data, url, simpleRequest)
{
    ajaxUtil(data, url, 'POST', simpleRequest);
}

function ajaxGet(data, url, simpleRequest)
{
    ajaxUtil(data, url, 'GET', simpleRequest);
}

function ajaxUtil(data, url, method, simpleRequest)
{
    $.ajax({
        url : url,
        type : method,
        dataType: "json",
        data: data,
        success: function (data) {
            if(data.status)
                simpleRequest.successCallback(data);
            else
                simpleRequest.failureCallback(data);
        },
        error: function (jqXhr, textStatus, errorThrown) {
            simpleRequest.errorCallback(jqXhr, textStatus, errorThrown);
        },
        complete: function(){
            if($.isFunction(simpleRequest.completeCallback))
                simpleRequest.completeCallback();
        }
    });
}