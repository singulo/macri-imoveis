/* ----- menu-2 ----- */ /* ----- detalhes-condominio-1 ----- */ var configCarousel = {
    nav: false,
    autoplay: true,
    loop: false, // O LOOP FODE O MAGNIFIC!
    items: 2,
    margin: 10,
    responsive : {
        320:{
            items:1,
            margin: 5
        },
        528:{
            items: 2,
            margin: 5
        },
        768:{
            items: 3,
            margin: 10
        },
        1000:{
            items: 3,
            margin: 5,
        }
    }
};

var configMagnificPopupImagem = {
    delegate: 'a',
    type: 'image',
    tLoading: 'Carregando imagem #%curr%...',
    mainClass: 'mfp-with-zoom',
    tClose: 'Fechar (Esc)',
    gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1], // Will preload 0 - before current, and 1 after the current image
        tPrev: 'Voltar (Seta para esquerda)',
        tNext: 'Próximo (Seta para direita)',
        tCounter: '%curr% de %total%'
    },
    image: {
        tError: '<span>Imagem não encontrada</span>',
        titleSrc: function(item) {
            return item.el.attr('title');
        }
    }
};

var configMagnificPopupVideos = {
    delegate: 'a',
    disableOn: 700,
    type: 'iframe',
    tLoading: 'Carregando video #%curr%...',
    mainClass: 'mfp-fade',
    tClose: 'Fechar (Esc)',
    removalDelay: 160,
    preloader: false,
    gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1], // Will preload 0 - before current, and 1 after the current image
        tPrev: 'Voltar (Seta para esquerda)',
        tNext: 'Próximo (Seta para direita)',
        tCounter: '%curr% de %total%'
    },
    fixedContentPos: false
};

$('.nav-tabs li').on('click', function(){

    $('.tab-content .tab-pane').hide();

    $($(this).find('a').attr('href')).fadeIn();
});

$(document).ready(function() {

    if ($('.detalhes-condominio-1 #galeria-fotos').size() > 0)
        $('.detalhes-condominio-1 #galeria-fotos').owlCarousel(configCarousel);

    if ($('.detalhes-condominio-1 #galeria-plantas').size() > 0)
        $('.detalhes-condominio-1 #galeria-plantas').owlCarousel(configCarousel);

    if ($('.detalhes-condominio-1 #galeria-videos').size() > 0)
        $('.detalhes-condominio-1 #galeria-videos').owlCarousel(configCarousel);

    if ($('.detalhes-condominio-1 #galeria-fotos').size() > 0)
        $('.detalhes-condominio-1 #galeria-fotos').magnificPopup(configMagnificPopupImagem);

    if ($('.detalhes-condominio-1 #galeria-plantas').size() > 0)
        $('.detalhes-condominio-1 #galeria-plantas').magnificPopup(configMagnificPopupImagem);

    if ($('.detalhes-condominio-1 #galeria-videos').size() > 0)
        $('.detalhes-condominio-1 #galeria-videos').magnificPopup(configMagnificPopupVideos);
});/* ----- rodape-2 ----- */ 