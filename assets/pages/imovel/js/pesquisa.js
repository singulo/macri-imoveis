/* ----- menu-2 ----- */ /* ----- resultado-pesquisa-1 ----- */ /* ----- pesquisa-avançada-1 ----- */ $(document).ready(function(){
    // VALOR SLIDER
    $('.slider-filtro').slider({
        formatter: function(value) {

            if(value[0] != undefined)
                atualiza_label_no_slider_valor_imovel_e_formatted(value[0], value[1]);

            if(value[1] == 3000000)
                return formata_valor_imovel(value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return formata_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $(".slider-filtro").on("slide", function(slideEvt) {
        atualiza_label_no_slider_valor_imovel_e_formatted(slideEvt.value[0], slideEvt.value[1]);
    });

    // SIMULA EVENTO DE MUDANÇA PARA SELECIONAR CORRETAMENTE OS CONDOMINIOS DA CIDADE SELECIONADA
    $('.pesquisa-avancada-1 #filtro-imovel select.selectpicker[name="cidade"]').trigger("change");
});

function atualiza_label_no_slider_valor_imovel_e_formatted(valMin, valMax)
{
    $(' input[name="preco_min"]').val(valMin);
    $(' input[name="preco_max"]').val(valMax);

    var mostrar_mais = '';
    if(valMax == 3000000)
        mostrar_mais = '+';

    $('.filtro-valores').html('<span class="pull-left valor-minimo">' + formata_valor_imovel(valMin, '<small> R$</small>') +'</span><span class="pull-right">' + formata_valor_imovel(valMax, '<small>R$</small>') + mostrar_mais + '</span>');
}


function formata_valor_imovel(valor_imovel, cifrao)
{
    if(valor_imovel == 'Consulte')
        return valor_imovel;
    else
    {
        var e = $('<div></div>');
        e.autoNumeric('init', {aSep: '.', aDec: ',', vMax: 999999999.99});

        var valor = e.autoNumeric('set', valor_imovel);
        return cifrao + valor.text();
    }
}



$(document).ready(function(){
    //VALOR SLIDER
    $('.slider-metragem').slider({
        formatter: function(value) {

            if(value[0] != undefined)
                atualiza_label_no_slider_e_formatted(value[0], value[1]);

            if(value[1] == 300)
                return formata_valor_imovel(value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return formata_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $(".slider-metragem").on("slide", function(slideEvt) {
        atualiza_label_no_slider_e_formatted(slideEvt.value[0], slideEvt.value[1]);
    });
});

function atualiza_label_no_slider_e_formatted(valMin, valMax)
{
    $(' input[name="metragem_min"]').val(valMin);
    $(' input[name="metragem_max"]').val(valMax);

    var mostrar_mais = '';
    if(valMax == 300)
        mostrar_mais = '+';


    $('.filtro-metragem').html('<span class="pull-left valor-minimo">' + formata_metragem_imovel(valMin, '<small>m²</small>') + '</span><span class="pull-right">' + formata_metragem_imovel(valMax, '<small>m²</small>') + mostrar_mais + '</span>');
}

function formata_metragem_imovel(tamanho, cifrao)
{
    return tamanho + cifrao;
}

$('.pesquisa-avancada-1 #filtro-imovel select.selectpicker[name="cidade"]').on('change', function(){
    var form = $(this).closest('form');
    var selectCondominios = $('.pesquisa-avancada-1 #filtro-imovel select.selectpicker[name="id_condominio"]');
    var selectCidades = $(this).val();

    //HABILITA APENAS OS CONDOMINIOS DA CIDADE SELECIONADA
    if(selectCidades != '' && selectCidades != null)
    {
        $(selectCondominios).find('option').attr('disabled', true);
        $.each(selectCidades, function(index, cidade) {
            $(selectCondominios).find('option[data-subtext="' + cidade + '"]').attr('disabled', false);
        });

        console.log($(selectCondominios).find('option'));
        $.each($(selectCondominios).find('option'), function(index, option) {
            console.log(option.selected);

            if(option.selected && selectCidades.indexOf($(option).data('subtext')) == -1)
                option.selected = false
        });
    }
    else
    {
        $(selectCondominios).find('option').attr('disabled', false);
    }

    $(selectCondominios).selectpicker('refresh');
});/* ----- rodape-2 ----- */ 