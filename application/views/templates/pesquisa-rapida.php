<div class="container pesquisa-rapida-1">
    <div class="row">
        <div class="col-md-4">
            <a href="http://www.imobiliariamacri.com.br/imovel/pesquisar?finalidade=1&preco_min=0&preco_max=3000000&dormitorios=&banheiros=&suites=&garagem=&id=&pagina=0&limite=12&ordenacao=valor+ASC">
                <div class="busca-rapida">
                    <div class="col-md-12 tipo-destaque" style="background-image: url(http://www.imobiliariamacri.com.br/assets/images/img-venda.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                        <div class="row background">
                            <span>Venda</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.imobiliariamacri.com.br/imovel/pesquisar?id_tipo%5B%5D=7&id_tipo%5B%5D=10&preco_min=0&preco_max=3000000&dormitorios=&banheiros=&suites=&garagem=&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC">
                <div class="busca-rapida">
                    <div class="col-md-12 tipo-destaque" style="background-image: url(http://www.imobiliariamacri.com.br/assets/images/img-salas-comerciais.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                        <div class="row background">
                            <span>Comercial</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-4">
            <a href="http://www.imobiliariamacri.com.br/imovel/pesquisar?finalidade=3&preco_min=0&preco_max=3000000&dormitorios=&banheiros=&suites=&garagem=&id=&pagina=0&forcar_total=1&limite=12&ordenacao=valor+ASC">
                <div class="busca-rapida">
                    <div class="col-md-12 tipo-destaque" style="background-image: url(http://www.imobiliariamacri.com.br/assets/images/img-temporada.jpg), url(<?= base_url_filial('assets/images/imovel-sem-foto.jpg', false); ?>);">
                        <div class="row background">
                            <span>Temporada</span>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>