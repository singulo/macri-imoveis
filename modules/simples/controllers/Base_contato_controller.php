<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH."../modules/simples/core/Base_Controller.php";
require_once APPPATH . '../modules/simples/helpers/cliente_helper.php';
require_once APPPATH . '../modules/simples/helpers/email_helper.php';
require_once APPPATH . '../modules/simples/helpers/valor_imovel_formater_helper.php';
require_once APPPATH . '../modules/simples/libraries/RequestMapper.php';

/**
 * @property Contato_Model $contato_model
 * @property Contato_Imovel_Interesse_Model $contato_imovel_interesse_model
 * @property Clientes_Imoveis_Log_Model $clientes_imoveis_log_model
 * @property Clientes_Model $clientes_model
 * @property Corretores_Model $corretores_model
 * @property Imoveis_Model $imoveis_model
 */
class Base_Contato_Controller extends Base_Controller
{
	public function index()
	{
		$this->load->view('contato');
	}

	public function novo()
	{
		$resposta = array(
			'status' => false
		);

		$this->load->model('simples/contato_model');

		$mapper = array('criado_em' => array('default_value' => date("Y-m-d H:i:s")));

		$contato = RequestMapper::parseToObject($_POST, $mapper, new Contato_Model(), 'contato');

		$this->load->model('simples/clientes_model');
		$cliente = $this->clientes_model->pelo_email($contato->email);
		//CASO O INTERESSE SEJA ENVIADO DE UM EMAIL NAO CADASTRADO!
		if($cliente == NULL)
		{
			$cliente = new Clientes_Model();

			$cliente->id_corretor = $_SESSION['filial']['corretor_padrao_id'];

			$resposta['cliente_novo_registrado'] = registra_cliente($_POST, $_SESSION['filial']['corretor_padrao_id'], 'contato');
		}

		$this->load->model('simples/corretores_model');
		$corretor = $this->corretores_model->obter($cliente->id_corretor, 'id_corretor');

		if($this->contato_model->novo($contato) > 0)
		{
			$resposta['status'] = true;

			$resposta['status_email_notificacao_cliente'] = enviar_email($contato->email,
																		'Obrigado pelo contato!',
																		monta_corpo_email(array('nome' => $contato->nome, 'assunto' => 'Vamos lhe responder assim que possível.'),
																		'agradece_cliente_pelo_contato', strtolower($_SESSION['filial']['nome'])));

			$resposta['status_email_notificacao_gerente'] = enviar_email($_SESSION['filial']['email_padrao'],
																		'Cliente entrou em contato!',
																		monta_corpo_email(array('nome' => $corretor->nome, 'assunto' => 'Você acaba de receber um novo contato através do site, a seguir maiores detalhes.', 'cliente' => $contato->nome, 'email' => $contato->email, 'telefone' => $contato->telefone, 'contato_assunto' => $contato->assunto, 'contato_mensagem' => $contato->mensagem),
																		'notificacao_contato_cliente', strtolower($_SESSION['filial']['nome'])));
		}

		echo json_encode($resposta);
	}

	public function interesse_imovel()
	{
		$resposta = array(
			'status' => false
		);

		$this->load->model('simples/contato_imovel_interesse_model');

		/** @var Clientes_Model $cliente */
		$cliente = $this->session->userdata('usuario');

		/** @var Contato_Imovel_Interesse_Model $contato */
		$contato = new Contato_Imovel_Interesse_Model();
		$contato->cod_imovel = $_POST['cod_imovel'];
		$contato->email = $cliente->email;
		$contato->id_corretor = $cliente->id_corretor;
		$contato->nome = $cliente->nome;

		if(strlen($cliente->telefone_1) == 0)
			$cliente->telefone_1 = '';

		$contato->telefone = $cliente->telefone_1;
		$contato->obs = $_POST['obs'];
		$contato->criado_em = date("Y-m-d H:i:s");

		if($this->contato_imovel_interesse_model->inserir($contato) > 0)
		{
			$resposta['status'] = true;

			$this->load->model('simples/imovel_model');
			$imovel = $this->imovel_model->pelo_codigo($contato->cod_imovel);

			$imoveis_html = monta_div_imoveis_para_proposta(array($imovel));


			$resposta['status_email_notificacao_usuario'] = enviar_email($contato->email,
																			'Obrigado pelo interesse!',
																			monta_corpo_email(array('nome' 			=> $contato->nome,
																									'assunto' 		=> 'Você acaba de solicitar contato para saber mais sobre o imóvel código ' . $contato->cod_imovel . ', Aguarde o contato de um de nossos corretores. A seguir seus dados para contato.',
																									'cliente' => $contato->nome,
																									'email' => $contato->email,
																									'telefone' => $contato->telefone,
																									'obs' => $contato->obs,
																									'imoveis'		=> $imoveis_html),
																									'cliente_interesse_em_imovel',
																			strtolower($_SESSION['filial']['nome'])));

			$this->load->model('simples/corretores_model');
			$corretor = $this->corretores_model->obter_informacoes_basicas($contato->id_corretor);

			$resposta['status_email_notificacao_gerente'] = enviar_email($corretor->email,
																			'Cliente interessado em um imóvel!',
																			monta_corpo_email(array('nome' 		=> $corretor->nome,
																									'assunto' 	=> 'Um de seus clientes está interessado em um imóvel',
																									'cliente' 	=> $contato->nome,
																									'email' 	=> $contato->email,
																									'telefone' 	=> $contato->telefone,
																									'obs' 		=> $contato->obs,
																									'imoveis'	=> $imoveis_html),
																									'notificacao_de_cliente_interessado_em_imovel'));
		}

		echo json_encode($resposta);
	}
}
