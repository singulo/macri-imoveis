/* ----- menu-2 ----- */ /* ----- lista-condominios-1 ----- */ function mostrarCondominios() {
    var option = $('select[name=filtro_empreendimentos]').val();

    if (option == 1) {
        $('.condominios .condominio').fadeOut(800);

        setTimeout(function() { $('.condominios .condominio').fadeIn(900); }, 800);
    }
    else if (option == 2) {
        $('.condominios .condominio').fadeOut(800);

        setTimeout(function() { $('.condominios .condominio.lancamento').fadeIn(900); }, 800);

    } else {
        $('.condominios .condominio').fadeOut(800);
        setTimeout(function() { $('.condominios .condominio.fechado').fadeIn(900); }, 800);

    }
}/* ----- rodape-2 ----- */ 