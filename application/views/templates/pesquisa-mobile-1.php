<? require_once MODULESPATH . 'simples/helpers/form_values_helper.php'; ?>
<? require_once MODULESPATH . 'simples/libraries/Finalidades.php'; ?>

<? if( ! isset($filtro))
{
    $filtro = new stdClass();
    $filtro->preco_min = 0;
    $filtro->id = "";
    $filtro->preco_max = 3000000;
    $filtro->metragem_max = 300;
    $filtro->metragem_min = 0;
    $filtro->id_tipo = array();
    $filtro->id_condominio = array();
    $filtro->cidade = array();
    $filtro->finalidade = array();
    $filtro->dormitorios = NULL;
    $filtro->garagem = NULL;
    $filtro->banheiros = NULL;
    $filtro->suites = NULL;
}
?>

<div class=" pesquisa-mobile-1 collapse" id="pesquisa-mobile">
    <div class="col-xs-12 painel-pesquisa-mobile">
        <div class="row form-pesquisa-avancada">
            <div class="col-xs-6 titulo-pesquisa">
                <div class="col-xs-12">
                    <div class="row">
                        <span class="pull-left">Pesquisa</span>
                    </div>
                </div>
                <hr>
            </div>
            <form class="form-filtro" id="filtro-imovel-mobile" action="<?= base_url_filial('imovel/pesquisar'); ?>">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-2 ">
                            <div class="form-group">
                                <label for="finalidade">Finalidade</label>
                                <select class="selectpicker" name="finalidade" multiple data-width="100%" title="Selecione">
                                    <? foreach( Finalidades::getConstants() as $finalidade => $valor ) : ?>
                                        <option value="<?= $valor;?>" <? if(select_value($valor, $filtro->finalidade)) echo 'selected'; ?>><?= $finalidade; ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6" data-width="100%">
                            <div class="form-group">
                                <label for="tipo">Tipo</label>
                                <select class="selectpicker" name="id_tipo" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['tipos_imoveis'] as $tipo) : ?>
                                        <option value="<?= $tipo->id; ?>" <? if(select_value($tipo->id, $filtro->id_tipo)) echo 'selected'; ?>><?= $tipo->tipo;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="cidade">Localização</label>
                                <select class="selectpicker" name="cidade" multiple data-width="100%" title="Selecione">
                                    <? foreach($_SESSION['filial']['cidades_imoveis'] as $cidade) : ?>
                                        <option value="<?= $cidade->cidade; ?>" <? if(select_value($cidade->cidade, $filtro->cidade)) echo 'selected'; ?> ><?= $cidade->cidade;?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group ">
                                <label for="dormitorios">Dormitórios</label>
                                <select name="dormitorios" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['dormitorios']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->dormitorios) && $i == $filtro->dormitorios) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group ">
                                <label for="banheiros">Banheiros</label>
                                <select name="banheiros" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= 10; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->banheiros) && $i == $filtro->banheiros) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group ">
                                <label for="suites">Suítes</label>
                                <select name="suites" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['suites']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->suites) && $i == $filtro->suites) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group ">
                                <label for="garagem">Vagas</label>
                                <select name="garagem" class="selectpicker" data-width="100%">
                                    <option value="" selected>-</option>
                                    <? for ($i = 0; $i <= $this->config->item('pesquisa')['vagas']; $i++) : ?>
                                        <option value="<?= $i; ?>" <? if(is_numeric($filtro->garagem) && $i == $filtro->garagem) echo 'selected'; ?>><?= $i; ?></option>
                                    <? endfor; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 slider-valores">
                            <div class="form-group">
                                <? $filtro_valor_min = array('valor' => $filtro->preco_min, 'formatado' => number_format((int)$filtro->preco_min, 2, ',', '.')); ?>
                                <? $filtro_valor_max = array('valor' => $filtro->preco_max, 'formatado' => number_format((int)$filtro->preco_max, 2, ',', '.')); ?>
                                <input type="hidden" name="preco_min" value="<?= $filtro_valor_min['valor']; ?>">
                                <input type="hidden" name="preco_max" value="<?= $filtro_valor_max['valor']; ?>">
                                <div class="legendas">
                                    <span>Valor</span>
                                </div>
                                <input type="text" class="slider-filtro" data-slider-tooltip="hide" data-slider-min="0" data-slider-max="3000000" data-slider-step="50000" data-slider-value="[<?= $filtro_valor_min['valor']; ?>, <?= $filtro_valor_max['valor']; ?>]"/>
                                <div class="filtro-valores">
                                    <span class="pull-left"><small>R$</small> <?= $filtro_valor_min['formatado']; ?> a</span>

                                    <!--                                <span class="pull-left">Minimo</span>-->
                                    <!--                                <span class="pull-right">Máximo </span>-->
                                    <span class="pull-right"><small>R$</small><?= $filtro_valor_max['formatado']; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="complementos">Condomínios</label>
                                <select class="selectpicker" name="id_condominio" multiple data-width="100%" title="Selecione">
                                    <? foreach ($_SESSION['filial']['condominios'] as $condominio) : ?>
                                        <option value="<?= $condominio->id; ?>"  data-subtext="<?= $condominio->cidade; ?>" <? if(select_value($condominio->id, $filtro->id_condominio)) echo 'selected'; ?> ><?= $condominio->nome;?></option>
                                    <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="id">Código do imóvel</label>
                                <input type="text" class="form-control codigo-imovel" name="id" value="<?= $filtro->id; ?>" placeholder="Digite o código">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <button type="button" onclick="pesquisar(0);" class="btn-success btn-pesquisar"><i class="fa fa-search"></i> Realizar Pesquisa</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var filtro = <?= json_encode($filtro); ?>;

</script>
