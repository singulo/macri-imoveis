/* ----- menu-2 ----- */ /* ----- mapa-1 ----- */ /* ----- form-contato ----- */ /* ----- condominios-1 ----- */ $(document).ready(function() {
    $('.logos-condominios').owlCarousel({
        nav: false,
        autoplay: true,
        loop: true,
        responsiveClass: true,
        responsive: {
            320: {
                items: 1,
                margin:5
            },
            768: {
                items: 3,
                margin: 10
            },
            1000: {
                items: 4,
                margin: 10,
            }
        }
    });
});/* ----- rodape-2 ----- */