$(document).ready(function(){
    // VALOR SLIDER
    $('.pesquisa-rapida-1 form.form-filtro .slider-filtro').slider({
        formatter: function(value) {

            if(value[0] != undefined)
                atualiza_label_no_slider_valor_imovel_e_formatted(value[0], value[1]);

            if(value[1] == 3000000)
                return formata_valor_imovel(value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return formata_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $("form.form-filtro-2 .slider-filtro").on("slide", function(slideEvt) {
        atualiza_label_no_slider_valor_imovel_e_formatted(slideEvt.value[0], slideEvt.value[1]);
    });
});

function atualiza_label_no_slider_valor_imovel_e_formatted(valMin, valMax)
{
    $('.pesquisa-rapida-1 form.form-filtro input[name="preco_min"]').val(valMin);
    $('.pesquisa-rapida-1 form.form-filtro input[name="preco_max"]').val(valMax);

    var mostrar_mais = '';
    if(valMax == 3000000)
        mostrar_mais = '+';

    $('.pesquisa-rapida-1 form.form-filtro .filtro-valores').html('<span class="pull-left valor-minimo">De ' + formata_valor_imovel(valMin, '<small> R$</small>') + ' a </span><span class="">' + formata_valor_imovel(valMax, '<small>R$</small>') + mostrar_mais + '</span>');
}


function formata_valor_imovel(valor_imovel, cifrao)
{
    if(valor_imovel == 'Consulte')
        return valor_imovel;
    else
    {
        var e = $('<div></div>');
        e.autoNumeric('init', {aSep: '.', aDec: ',', vMax: 999999999.99});

        var valor = e.autoNumeric('set', valor_imovel);
        return cifrao + valor.text();
    }
}



$(document).ready(function(){
    //VALOR SLIDER
    $('.pesquisa-rapida-1 form.form-filtro .slider-metragem').slider({
        formatter: function(value) {

            if(value[0] != undefined)
                atualiza_label_no_slider_e_formatted(value[0], value[1]);

            if(value[1] == 300)
                return formata_valor_imovel(value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1], '', '0,00') + '+';
            else
            {
                return formata_valor_imovel(value[0] == undefined ? 0 : value[0], '', '0,00') + ' : ' + formata_valor_imovel(value[1] == undefined ? 0 : value[1], '', '0,00');
            }
        }
    });

    $(".form-pesquisa-avancada form.form-filtro .slider-metragem").on("slide", function(slideEvt) {
        atualiza_label_no_slider_e_formatted(slideEvt.value[0], slideEvt.value[1]);
    });
});

function atualiza_label_no_slider_e_formatted(valMin, valMax)
{
    $('.form-pesquisa-avancada form.form-filtro input[name="metragem_min"]').val(valMin);
    $('.form-pesquisa-avancada form.form-filtro input[name="metragem_max"]').val(valMax);


    $('.form-pesquisa-avancada form.form-filtro .filtro-metragem').html('<span class="pull-left valor-minimo">Metragem de ' + formata_metragem_imovel(valMin, '<small> m²</small>') + ' a </span><span class="">' + formata_metragem_imovel(valMax, '<small>m²</small>') + '</span>');
}


function formata_metragem_imovel(tamanho, cifrao)
{

    return tamanho + cifrao;
}


