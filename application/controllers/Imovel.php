<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "../modules/simples/controllers/Base_imovel_controller.php";

class Imovel extends Base_Imovel_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->filtro_parametros[] = array('param' => 'bairro', 'default_value' => array());
    }

    public function index($imovel = NULL)
    {
        $data = parent::index($imovel);

        if( ! is_null($data['imovel']))
            $this->load->view('imovel/detalhe', $data);
        else
            die('Imóvel não encontrado ou não existe mais. Voltar a <a href="' . base_url() . ' ">página inicial</a>.');
    }

    public function pesquisar()
    {
        $data = parent::pesquisar();

        if(is_null($data['filtro']->preco_min) || $data['filtro']->preco_min == '')
            $data['filtro']->preco_min = 0;
        else
            $data['filtro']->preco_min = $this->transforma_valor_imovel($data['filtro']->preco_min);

        if(is_null($data['filtro']->preco_max) || $data['filtro']->preco_max == '')
            $data['filtro']->preco_max = 3000000;
        else
            $data['filtro']->preco_max = $this->transforma_valor_imovel($data['filtro']->preco_max);

        if(is_null($data['filtro']->metragem_min) || $data['filtro']->metragem_min == '')
            $data['filtro']->metragem_min = 0;

        if(is_null($data['filtro']->metragem_max) || $data['filtro']->metragem_max == '')
            $data['filtro']->metragem_max = 300;

        $this->load->view('imovel/pesquisa', $data);
    }


    public function buscar()
    {
        $data = parent::buscar();
        $data['status'] = true;

        echo json_encode($data);
    }

}
