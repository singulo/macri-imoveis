<!--Bootstrap-select-->
<script src="<?=base_url_filial('assets/plugins/bootstrap-select/js/bootstrap-select.js',false)?>"></script>
<script src="<?=base_url_filial('assets/plugins/bootstrap-select/js/defaults-pt_BR.js', false)?>"></script>

<!--Bootstrap-->
<script src="<?=base_url_filial('assets/plugins/bootstrap/js/bootstrap.js', false)?>"></script>

<!--Bootstrap-slider -->
<script src="<?=base_url_filial('assets/plugins/bootstrap-slider/js/bootstrap-slider.js',false)?>"></script>

<!--Alertify -->
<script src="<?=base_url_filial('assets/plugins/alertify/js/alertify.js',false)?>"></script>

<script>
    var imoveis_tipos = <?= json_encode($_SESSION['filial']['tipos_imoveis']); ?>;
    var finalidades = <?= json_encode(array_flip(Finalidades::getConstants())); ?>
</script>

<!--Simples assets-->
<script src="<?=base_url_filial('modules/simples/assets/js/autoNumeric.min.js',false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/jsonify.js',false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/imovel.js' ,false)?>"></script>

<!--Jquery-validation-->
<script src="<?=base_url_filial('assets/plugins/jquery-validate/jquery.validate.min.js',false)?>"></script>

<!--Jquery-pagination-->
<script src="<?=base_url_filial('assets/plugins/simplePagination/js/jquery.simplePagination.js',false)?>"></script>

<!--MaskMoney-->
<script src="<?=base_url_filial('assets/plugins/jquery.maskMoney/js/jquery.maskMoney.js', false)?>"></script>

<!-- jquery-mask -->
<script src="<?= base_url_filial('assets/plugins/jquery-mask/js/jquery.mask.min.js', false); ?>"></script>

<!--Magnific-Popup-->
<script src="<?=base_url_filial('assets/plugins/magnific-popup/js/jquery.magnific-popup.js', false)?>"></script>

<!--Google maps-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoXkNL9Vlwc_XtdQVor0psdbc5r1nQYtg"></script>

<script src="<?=base_url_filial('assets/js/custom.js', false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/cliente.js', false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/contato.js', false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/filtro-slider.js', false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/simples.js',false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/pesquisa.js',false)?>"></script>
<script src="<?=base_url_filial('modules/simples/assets/js/gmap.js',false)?>"></script>

<? if (isset($appendScripts)) : ?>
    <? foreach ($appendScripts as $script) :?>
        <script src="<?= $script; ?>"></script>
    <?endforeach; ?>
<? endif; ?>


<script src="<?=base_url_filial('assets/js/macri.js',false)?>"></script>


