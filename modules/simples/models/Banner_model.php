<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(__DIR__ . '/../core/Base_Model.php');

class Banner_Model extends Base_Model
{
    public $id;
    public $mensagem;
    public $valor;
    public $valor_decimal;
    public $link;
    public $codigo_imovel;

    protected $table = 'tb_banner';

    public function novo(Banner_Model $banner)
    {
        $this->db->insert($this->table, $banner);
        return $this->db->insert_id();
    }

    public function editar(Banner_Model $banner)
    {
        $this->db->where('id', $banner->id);
        $this->db->update($this->table, $banner);
        return $this->db->affected_rows();
    }

    public function adicionarOuEditar(Banner_Model $banner)
    {
        $data = array(
            'id'            => $banner->id,
            'mensagem'      => $banner->mensagem,
            'valor'         => $banner->valor,
            'valor_decimal' => $banner->valor_decimal,
            'link'          => $banner->link,
            'codigo_imovel' => $banner->codigo_imovel,
        );

        $sql = $this->db->insert_string($this->table, $data) . ' ON DUPLICATE KEY UPDATE id = id,  mensagem = VALUES(mensagem),  valor = VALUES(valor), valor_decimal = VALUES(valor_decimal), link = VALUES(link), codigo_imovel = VALUES(codigo_imovel)';
        $this->db->query($sql);
        return $this->db->affected_rows();
    }
}
